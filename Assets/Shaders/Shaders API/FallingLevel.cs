using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.ShadersAPI
{
    /// <summary>
    /// ������ ��������� �� ������ �������� ������, ������� ����� ������
    /// </summary>
    public class FallingLevel : MonoBehaviour
    {

        public event Action OnUp = delegate { };
        public event Action OnFall = delegate { };
        [SerializeField] float speedFalling = 1;
        [SerializeField] float acceleration = 5;
        float startPos;
        public float Height { get; set; }
       
        public float SpeedFalling
        {
            get
            {
                return speedFalling;
            }
            set
            {
                if (value > 0) speedFalling = value;
                else speedFalling = 0;
            }
        }

        public float Acceleration
        {
            get
            {
                return acceleration;
            }
            set
            {
                if (value > 0) acceleration = value;
                else acceleration = 0;
            }
        }

        void Awake()
        {
            startPos = transform.localPosition.y;
        }

        /// <summary>
        /// ������� ������� ������� �������� ������
        /// </summary>
        /// <param name="fallback">������� ���������,���� ����� ��������� ����� ��������</param>
        public void FallingAnimation(Action fallback = null)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, Height, transform.localPosition.z);
            StartCoroutine(AnimationFalling(fallback));
        }

        IEnumerator AnimationFalling(Action fallback = null)
        {
            var maxSpeedChange = speedFalling;
            while (transform.localPosition.y > startPos)
            {
                maxSpeedChange +=  acceleration * Time.deltaTime;
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, 
                    new Vector3(transform.localPosition.x, startPos, transform.localPosition.z), maxSpeedChange);

                yield return null;
            }
            OnFall();
            fallback?.Invoke();
        }
    }
}
