Shader "HPEW/Skybox/Day and night Cubemap" {
    Properties{
        _Tint("Tint Color", Color) = (.5, .5, .5, .5)
        [Gamma] _Exposure("Exposure", Range(0, 8)) = 1.0
        _Speed("Rotation", Float) = 0
        [NoScaleOffset] _TexDay("Cubemap day   (HDR)", Cube) = "grey" {}
        [NoScaleOffset] _TexNight("Cubemap night  (HDR)", Cube) = "grey" {}
        [NoScaleOffset] _TexMorning("Cubemap morning   (HDR)", Cube) = "grey" {}
        [NoScaleOffset] _TexAfternoon("Cubemap afternoon  (HDR)", Cube) = "grey" {}
        _LerpDayAndNight("Day in night value",Range(0,4)) = 0
    }

        SubShader
    {
        Tags { "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
        Cull Off ZWrite Off

        Pass {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"

            samplerCUBE _TexDay;
            half4 _TexDay_HDR;
            samplerCUBE _TexNight;
            half4 _TexNight_HDR;
            samplerCUBE _TexMorning;
            half4 _TexMorning_HDR;
            samplerCUBE _TexAfternoon;
            half4 _TexAfternoon_HDR;
            half4 _Tint;
            half _Exposure;
            float _Speed;
            float _LerpDayAndNight;

            float3 RotateAroundYInDegrees(float3 vertex, float degrees)
            {
                float alpha = degrees * UNITY_PI / 180.0;
                float sina, cosa;
                sincos(alpha, sina, cosa);
                float2x2 m = float2x2(cosa, -sina, sina, cosa);
                return float3(mul(m, vertex.xz), vertex.y).xzy;
            }

            struct appdata_t {
                float4 vertex : POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                float3 texcoord : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2f vert(appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                float3 rotated = RotateAroundYInDegrees(v.vertex, (_Time.y * _Speed) % 360);
                o.vertex = UnityObjectToClipPos(rotated);
                o.texcoord = v.vertex.xyz;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {

                half4 texDay = texCUBE(_TexDay, i.texcoord);
                half4 texNight = texCUBE(_TexNight, i.texcoord);
                half4 texMorning = texCUBE(_TexMorning, i.texcoord);
                half4 texAfternoon = texCUBE(_TexAfternoon, i.texcoord);
                half3 cDay = DecodeHDR(texDay, _TexDay_HDR);
                half3 cNight = DecodeHDR(texNight, _TexNight_HDR);
                half3 cMorning = DecodeHDR(texMorning, _TexMorning_HDR);
                half3 cAfternoon = DecodeHDR(texAfternoon, _TexAfternoon_HDR);

                half3 c = (half3)0;

                if (_LerpDayAndNight > 3)
                {
                    c = lerp(cMorning, cDay, _LerpDayAndNight - 3);
                }
                else if (_LerpDayAndNight > 2)
                {
                    c = lerp(cNight, cMorning, _LerpDayAndNight - 2);
                }
                else if (_LerpDayAndNight > 1)
                {
                    c = lerp(cAfternoon, cNight, _LerpDayAndNight - 1);
                }
                else
                {
                    c = lerp(cDay, cAfternoon, _LerpDayAndNight);
                }

                c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
                c *= _Exposure;
                return half4(c, 1);
            }
            ENDCG
        }
    }


        Fallback Off

}