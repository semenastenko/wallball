﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.Interactive
{
    public class Coins : Buster
    {
        protected override void Awake()
        {
            base.Awake();
        }

        public override void Restart()
        {

        }

        public override void Sleep()
        {

        }

        public override void Interact()
        {
            Debug.Log("Interact Coins");
            resourses.ChangeCoins(1);
            InvokeOnDestroyed();
        }
    }
}
