﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPEW.UI;

public class Score
{
    public float CurrentScore { get; private set; }
    public float LastScore { get; private set; }
    public float RecordScore { get; private set; }

    SaveData _saveData;
    UIConteiner UI;

    public Score(SaveData saveData, UIConteiner UI)
    {
        _saveData = saveData;
        this.UI = UI;

        _saveData.Load();

        RecordScore = (float)saveData.GetSave("RecordScore", 0f);
        LastScore = (float)saveData.GetSave("LastScore", 0f);
        UI.recordScoreText.text = RecordScore.ToString();
        UI.lastScoreText.text = LastScore.ToString();

    }

    public void SetScore(float value)
    {
        if (value >= 0)
            CurrentScore = value;

        if (CurrentScore > RecordScore)
        {
            RecordScore = CurrentScore;
            UI.recordScoreText.text = RecordScore.ToString();
            _saveData.SetSave("RecordScore", RecordScore);
            _saveData.Save();
        }

        UI.scoreText.text = CurrentScore.ToString();
    }

    public void SetLastScore(float value)
    {
        if (value >= 0)
            LastScore = value;

        UI.lastScoreText.text = LastScore.ToString();
        _saveData.SetSave("LastScore", LastScore);
        _saveData.Save();
    }

}
