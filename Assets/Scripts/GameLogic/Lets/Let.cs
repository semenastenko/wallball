﻿using System;
using UnityEngine;
using Zenject;

namespace HPEW.Interactive 
{
    public abstract class Let : MonoBehaviour
    {
        public int HP { get; protected set; } = 1;

        public bool isInteractive { get; protected set; } = true;

        public event Action OnDestroyed = delegate { };

        protected virtual void Awake() 
        {

        }

        /// <summary>
        /// Взаимодействие с препятствием
        /// </summary>
        /// <param name="data"></param>
        public abstract void Interact(object data = null);

        public abstract void AutoInteract(object data = null);

        public abstract void Restart();

        public abstract void Sleep();

        public void ImmediatelyDestroy()
        {
            InvokeOnDestroyed();
        }

        protected void InvokeOnDestroyed()
        {
            OnDestroyed?.Invoke();
        }
    }
}