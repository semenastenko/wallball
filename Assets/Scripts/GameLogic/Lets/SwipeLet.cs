﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Zenject;
using UniRx;
using HPEW.Input;
using HPEW.ShadersAPI;
using HPEW.Systemic;

namespace HPEW.Interactive
{
    public class SwipeLet : Let
    {
        [SerializeField] float distance = 5f;
        Vector3 direction = Vector3.zero;

        [SerializeField] float speed = 10f;

        float speedDissolve;

        //Image _arrow;
        TypeSwipe reference;

        //DissolveAPI _dissolve;
        FallingLevel fallingLevel;

        [Inject] Settings _settings;

        [SerializeField] GameObject _arrow;
        [SerializeField] GameObject upParticles;
        [SerializeField] GameObject downParticles;
        [SerializeField] GameObject leftParticles;
        [SerializeField] GameObject rightParticles;

        protected override void Awake()
        {
            base.Awake();
            //_arrow = GetComponentInChildren<Image>();
            //_dissolve = GetComponentInChildren<DissolveAPI>(true);
            fallingLevel = GetComponentInChildren<FallingLevel>(true);
            speedDissolve = _settings.SpeedDissolveSwipeLet;
        }


        public override void Restart()
        {
            _arrow.SetActive(false);
            var enums = Enum.GetValues(typeof(TypeSwipe));

            reference = (TypeSwipe)enums.GetValue(Random.Range(0, enums.Length));
            SetDirectionArrow(reference);

            isInteractive = true;

            //_dissolve.ChangeDissolveValue = 1;
            //_dissolve.StartAnimationAppear(speedDissolve);
            fallingLevel.Height = 75;
            Action onFalled = null;
            onFalled = delegate
            {
                _arrow.SetActive(true);
                fallingLevel.OnFall -= onFalled;
            };
            //fallingLevel.Acceleration = Random.Range(0.1f, 0.981f);
            fallingLevel.OnFall += onFalled;
            fallingLevel.FallingAnimation();
        }

        public override void Sleep()
        {

        }


        public override void Interact(object data = null)
        {
            var typeSwipe = (TypeSwipe)data;
            if (typeSwipe != reference) return;

            isInteractive = false;
            InvokeOnDestroyed();
            Observable.FromMicroCoroutine(_ => Move())
                .Subscribe();
        }

        public override void AutoInteract(object data = null)
        {
            if (!(data is AutogameBuster)) return;
            isInteractive = false;
            InvokeOnDestroyed();
            Observable.FromMicroCoroutine(_ => Move())
                .Subscribe();
        }

        private IEnumerator Move()
        {
            float t = 0;

            var start = transform.position;
            var end = transform.position + direction * distance;

            while (transform.position != end)
            {
                transform.position = Vector3.Lerp(start, end, t);

                t += Time.deltaTime * speed;

                yield return null;
            }
        }

        private void SetDirectionArrow(TypeSwipe type)
        {
            var rotation = _arrow.transform.localRotation;

            upParticles.SetActive(false);
            downParticles.SetActive(false);
            leftParticles.SetActive(false);
            rightParticles.SetActive(false);

            switch (type)
            {
                case TypeSwipe.Left:
                    rotation = Quaternion.Euler(0, 0, 90);
                    direction = -transform.right;
                    leftParticles.SetActive(true);
                    break;
                case TypeSwipe.Right:
                    rotation = Quaternion.Euler(0, 0, -90);
                    direction = transform.right;
                    rightParticles.SetActive(true);
                    break;
                case TypeSwipe.Up:
                    rotation = Quaternion.Euler(0, 0, 0);
                    direction = transform.up;
                    upParticles.SetActive(true);
                    break;
                case TypeSwipe.Down:
                    rotation = Quaternion.Euler(0, 0, 180);
                    direction = -transform.up;
                    downParticles.SetActive(true);
                    break;
            }

            _arrow.transform.localRotation = rotation;
        }

    }
}
