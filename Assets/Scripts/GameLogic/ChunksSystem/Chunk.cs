﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Zenject;
using UniRx;
using HPEW.Systemic;
using HPEW.Interactive;
using HPEW.Pool;
using HPEW.ShadersAPI;

namespace HPEW.ChunkSystem
{
    public class Chunk : MonoBehaviour
    {
        [SerializeField] protected Transform _begin;
        [SerializeField] protected Transform _end;
        public Transform Begin => _begin;
        public Transform End => _end;


        [Inject] protected Settings _settings;
        [Inject] protected DiContainer _container;
        [Inject] PoolLet[] _poolLets;
        [Inject] BusterSystem _busterSpawner;
        [Inject] ShellSystem.Ghost _ghost;

        List<Let> lets;
        Dictionary<Let, int> openlets;

        int interval;

        private static bool isImmideatly = false;

        public int LetsPerChunk
        {
            set
            {
                if (value < 10 && value > 0)
                    interval = 100 / value;
                else
                    interval = 0;
            }
            get
            {
                return 100 / interval;
            }
        }

        //DissolveAPI _dissolve;
        FallingLevel fallingLevel;
        float speedDissolve;
        float despawnDelay = 2.5f;

        IDisposable timerDispose;

        //Action handler = null;
        protected virtual void Awake()
        {
            //_dissolve = GetComponent<DissolveAPI>();
            fallingLevel = GetComponentInChildren<FallingLevel>();

            interval = _settings.IntervalLets;
            speedDissolve = _settings.SpeedDissolveChunk;

            lets = new List<Let>();
            openlets = new Dictionary<Let, int>();

            //Create();
        }

        protected virtual void Start()
        {
            //_dissolve.ChangeDissolveValue = 1f;
            //_dissolve.StartAnimationAppear(speedDissolve);
            fallingLevel.Height = 75;
            fallingLevel.FallingAnimation();
        }

        public virtual void Create()
        {
            isImmideatly = false;
            CreateRandom();
        }

        protected virtual void CreateRandom()
        {
            if (_poolLets.Length == 0 || interval == 0) return;
            despawnDelay = 5f;
            List<int> randomList = new List<int>();
            for (int i = -50; i < 50; i += interval)
            {
                randomList.Add(i);
            }

            timerDispose = Observable.Timer(TimeSpan.FromSeconds(0.1f))
                .Repeat()
                .Subscribe(_ =>
                {
                    var currentPos = Random.Range(0, randomList.Count);
                    var indexPool = Random.Range(0, _poolLets.Length);

                    var let = _poolLets[indexPool].Spawn();

                    var parent = let.transform.parent;
                    let.transform.SetParent(transform);
                    let.transform.localPosition = new Vector3(0, 2.5f, randomList[currentPos]);
                    randomList.RemoveAt(currentPos);
                    lets.Add(let);

                    _ghost.EnableLastGhost(transform.position);
                    _ghost.EnableRecordGhost(transform.position);

                    Action handler = null;
                    Action<bool> immideatlyHandler = (x) =>
                    {
                        let.OnDestroyed -= handler;

                        let.transform.SetParent(transform.parent);
                        openlets.Add(let, indexPool);
                        var dispose = Observable.Timer(TimeSpan.FromSeconds(despawnDelay))
                        .Subscribe(delegate
                        {
                            if (openlets.ContainsKey(let))
                            { 
                                lets.Remove(let);
                                openlets.Remove(let);
                                _poolLets[indexPool].Despawn(let);
                            }
                        });

                        if (x)
                        {

                            Debug.Log("Dispose");
                            dispose?.Dispose();
                            //lets.Remove(let);
                            openlets.Remove(let);
                            _poolLets[indexPool].Despawn(let);
                        }
                    };
                    handler = () => immideatlyHandler?.Invoke(isImmideatly);

                    let.OnDestroyed += handler;
                    if (randomList.Count == 0)
                    {
                        timerDispose?.Dispose();
                    }
                });

            //if (Random.Range(0, 5) == 0)
            {
                var buster = _busterSpawner.Spawn();
                buster.transform.SetParent(transform);
                buster.transform.localPosition = new Vector3(Random.Range(3, 9) * (Random.Range(0, 2) == 0 ? -1 : 1), Random.Range(6, 10), Random.Range(-25, 25) * 2);
            }
        }

        public void ImmediatelyDespawn()
        {
            _ghost.DisableLastGhost();
            _ghost.DisableRecordGhost();
            //despawnDelay = 0;
            isImmideatly = true;
            Debug.Log(lets);
            //if (lets.Count == 0) return;
            foreach (var let in lets)
            {
                let.ImmediatelyDestroy();
            }
            lets.Clear();
            foreach (var let in openlets)
            {
                _poolLets[let.Value].Despawn(let.Key);
            }
            openlets.Clear();
        }

        public void Destroy()
        {
            foreach (var let in lets)
            {
                let.ImmediatelyDestroy();
            }
            Destroy(gameObject);
        }
    }
}