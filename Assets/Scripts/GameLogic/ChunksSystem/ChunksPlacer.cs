﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using HPEW.Systemic;
using HPEW.ShellSystem;
using HPEW.Interactive;

namespace HPEW.ChunkSystem
{
    public class ChunksPlacer
    {
        Chunk[] _chunkPrefabs;

        List<Chunk> _spawnedChunks;

        Transform _player;

        int countInitChunks = 0;
        int maxCountChunks = 1;

        int chunkPlacedCount = 0;
        int nextLimit = 0;
        int currentLimit;
        int minLimit = 2;
        int maxLimit = 10;

        bool isBusted = false;

        DiContainer _container;
        Settings _settings;

        BusterSystem _busterSpawner;

        public ChunksPlacer(DiContainer container, Assets assets, Settings settings, Shell shell, BusterSystem busterSpawner)
        {
            _container = container;
            _chunkPrefabs = assets.ChunkPrefabs;
            _player = shell.transform;
            _busterSpawner = busterSpawner;
            _settings = settings;

            _busterSpawner.OnBusterInteract += ActivateCoinsBagBuster;

            countInitChunks = settings.CountInitChunks;
            maxCountChunks = settings.MaxCountChunks;

            chunkPlacedCount = 0;
            nextLimit = nextLimit = Random.Range(minLimit, maxLimit);

            if (countInitChunks + 1 > maxCountChunks)
            {
                Debug.LogError("Count Init Chunks + start chunk > max Count Chunks");
                return;
            }

            if (countInitChunks == 0)
            {
                Debug.LogError("CountInitChunks it`s 0!");
                return;
            }

            _spawnedChunks = new List<Chunk>();

            var firstChunk = Object.FindObjectOfType<Chunk>();

            if (firstChunk == null)
            {
                Debug.LogError("No initial chunk on stage");
                return;
            }

            _spawnedChunks.Add(Object.FindObjectOfType<Chunk>());
            for (int i = 0; i < countInitChunks; i++)
            {
                CreateChunk();
            }

            Observable.EveryUpdate()
                .Subscribe(delegate
                {
                    if (_player.position.z > _spawnedChunks[1].End.position.z)
                    {
                        CreateChunk();
                    }

                });
        }


        private void CreateChunk()
        {
            chunkPlacedCount++;
            var newChunk = _container.InstantiatePrefab(_chunkPrefabs[Random.Range(0, _chunkPrefabs.Length)])
                .GetComponent<Chunk>();
            newChunk.LetsPerChunk = GetLetsPerChunk();
            newChunk.Create();

            newChunk.transform.position = _spawnedChunks[_spawnedChunks.Count - 1].End.position - newChunk.Begin.localPosition;

            _spawnedChunks.Add(newChunk);

            newChunk.gameObject.name = "Chunk " + chunkPlacedCount;

            if (_spawnedChunks.Count > maxCountChunks)
            {
                Buster[] busters = _spawnedChunks[0].GetComponentsInChildren<Buster>();
                foreach (var buster in busters)
                {
                    buster.DestroyBuster();
                }
                if (!(_spawnedChunks[0] is StartChunk))
                    //Object.Destroy(_spawnedChunks[0].gameObject);
                    _spawnedChunks[0].Destroy();
                _spawnedChunks.RemoveAt(0);
            }
        }

        private int GetLetsPerChunk()
        {
            if (chunkPlacedCount == 1)
                return 1;
            if (chunkPlacedCount > 2 && chunkPlacedCount < nextLimit)
            {
                return currentLimit;
            }
            else
            {
                if (currentLimit == 5)
                    nextLimit = chunkPlacedCount + Random.Range(1, minLimit / 2);
                else
                    nextLimit = chunkPlacedCount + Random.Range(minLimit, maxLimit);

                if (nextLimit <= maxLimit)
                    currentLimit = 2;
                else
                {
                    int rand = Random.Range(2, 6);

                    currentLimit = rand == 3 ? 2 : rand;
                }
                return currentLimit;
            }
        }

        public void ResetChunk()
        {
            chunkPlacedCount = 0;
            nextLimit = nextLimit = Random.Range(minLimit, maxLimit);

            foreach (var item in _spawnedChunks)
            {
                if (!(item is StartChunk))
                {
                    item.ImmediatelyDespawn();
                    //Object.Destroy(item.gameObject);
                    item.Destroy();
                }
            }
            _spawnedChunks.Clear();

            var firstChunk = Object.FindObjectOfType<StartChunk>();

            if (firstChunk == null)
            {
                Debug.LogError("No initial chunk on stage");
                return;
            }

            _spawnedChunks.Add(Object.FindObjectOfType<StartChunk>());
            for (int i = 0; i < countInitChunks; i++)
            {
                CreateChunk();
            }

            isBusted = false;
        }

        public void DestroyBusters()
        {
            foreach (var chunk in _spawnedChunks)
            {
                Buster[] busters = chunk.GetComponentsInChildren<Buster>();
                foreach (var buster in busters)
                {
                    buster.DestroyBuster();
                }
            }
        }

        private void ActivateCoinsBagBuster(object data, System.Action action)
        {
            if (!(data is CoinsbagBuster)) return;
            isBusted = true;
            DestroyBusters();

            foreach (var chunk in _spawnedChunks)
            {
                SpawnCoins(chunk, data);
            }
            if (_busterSpawner.BustedBag < _settings.InitialSize)
                Observable.FromMicroCoroutine(_ => Delay(data, action))
               .Subscribe();
            else
                Observable.FromMicroCoroutine(_ => WaitShell(action))
              .Subscribe();
        }

        IEnumerator Delay(object data, System.Action action)
        {
            while ((_player.position.z > _spawnedChunks[1].End.position.z))
            {
                if (!isBusted) break;
                yield return null;
            }
            if (isBusted)
            {
                var chunk = _spawnedChunks[_spawnedChunks.Count - 1];
                SpawnCoins(chunk, data);
            }
            Observable.FromMicroCoroutine(_ => WaitShell(action))
              .Subscribe();
        }

        IEnumerator WaitShell(System.Action action)
        {
            var targetPos = _spawnedChunks[_spawnedChunks.Count - 1].transform.position.z + 50;
            while (_player.transform.position.z < targetPos)
            {
                if (!isBusted) break;
                yield return null;
            }
            action?.Invoke();
            isBusted = false;

        }

        private void SpawnCoins(Chunk chunk, object data)
        {
            var shellPos = _player.position.z;
            var chunkPos = chunk.transform.position.z;
            var distance = -(chunkPos - shellPos);
            var clampPos = (int)Mathf.Clamp(distance, -50, 50) + 10;
            for (int i = clampPos; i < 50; i += (data as CoinsbagBuster).Density)
            {
                if (_busterSpawner.BustedBag == _settings.InitialSize)
                    break;
                var buster = _busterSpawner.Spawn(true);
                buster.transform.SetParent(chunk.transform);
                buster.transform.localPosition = new Vector3(Random.Range(3, 9) * (Random.Range(0, 2) == 0 ? -1 : 1), Random.Range(6, 10), i);
                _busterSpawner.BustedBag++;
                //Debug.Log("BustedBag " + _busterSpawner.BustedBag);
            }
        }
    }
}