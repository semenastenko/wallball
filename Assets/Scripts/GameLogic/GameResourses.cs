﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPEW.ChunkSystem;
using HPEW.Interactive;
using HPEW.UI;

public class GameResourses
{
    public UIConteiner UI { get; private set; }

    SaveData _saveData;

    public int Coins { get; private set; } = 0;
    public int MagneteCount { get; private set; } = 0;
    public int AutogameCount { get; private set; } = 0;
    public int CoinsBagCount { get; private set; } = 0;
    public GameResourses(UIConteiner UI, SaveData saveData)
    {
        this.UI = UI;
        _saveData = saveData;
        _saveData.Load();
        ChangeCoins((int)_saveData.GetSave("Coins", 0));
        ChangeMagneteCount((int)_saveData.GetSave("MagneteCount", 0));
        ChangeAutogameCount((int)_saveData.GetSave("AutogameCount", 0));
        ChangeCoinsBagCount((int)_saveData.GetSave("CoinsBagCount", 0));

        ChangeMagneteCount(5);
        ChangeAutogameCount(5);
        ChangeCoinsBagCount(5);
    }
    public void ChangeCoins(int count)
    {
        Coins += count;
        UI.coinsText.text = Coins.ToString();
        _saveData.SetSave("Coins", Coins);
        _saveData.Save();
    }
    public void ChangeMagneteCount(int count)
    {
        MagneteCount += count;
        UI.magnetCountText.text = MagneteCount.ToString();
        _saveData.SetSave("MagneteCount", MagneteCount);
        _saveData.Save();
    }
    public void ChangeAutogameCount(int count)
    {
        AutogameCount += count;
        UI.autogameCountText.text = AutogameCount.ToString();
        _saveData.SetSave("AutogameCount", AutogameCount);
        _saveData.Save();
    }
    public void ChangeCoinsBagCount(int count)
    {
        CoinsBagCount += count;
        UI.coinsbagCountText.text = CoinsBagCount.ToString();
        _saveData.SetSave("CoinsBagCount", CoinsBagCount);
        _saveData.Save();
    }
}
