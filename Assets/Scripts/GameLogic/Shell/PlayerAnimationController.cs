﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPEW.ShellSystem;
using Zenject;

public class PlayerAnimationController : MonoBehaviour
{
    private Animator animator;
    [SerializeField, Range(0f, 100f)]
    float targetSpeed = 10.0f;

    float defaultSpeed;
    public Animator Animator => animator;

    [HideInInspector] public new Transform transform;
    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        transform = animator.transform;
        defaultSpeed = animator.speed;
    }

    public void Play_Idle()
    {
        animator.SetTrigger("Idle");
    }


    public void Play_Run()
    {
        animator.SetTrigger("Run");
    }

    public void Play_Ressuraction()
    {
        //animator.SetTrigger("Resurract");
        animator.SetBool("Ressuraction", true);
    }

    public void Play_Sprint(bool f)
    {
        animator.SetBool("Sprint", f);
    }

    public void Play_Dress()
    {
        animator.SetTrigger("Dress");
    }

    public void Dispose(bool f)
    {
        animator.SetBool("Dispose", f);
    }

    public void AnimatoinSpeed(float speed, float max)
    {
        animator.speed = speed * (targetSpeed / max);
    }

    public void DefaultAnimatoinSpeed()
    {
        animator.speed = defaultSpeed;
    }
}
