﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.Input
{
    public class InputSystem
    {
        public delegate void Swipe(TypeSwipe type);
        public event Swipe OnSwipe = delegate { };

        public delegate void Touch(TouchType type);
        public event Touch OnTouch = delegate { };

        public delegate void RaycastTouch(GameObject obj);
        public event RaycastTouch OnRaycastTouch = delegate { };

        private InputSystem()
        {
            var swipeManager = new SwipeManager(x => OnSwipe(x));
            var touchManager = new TouchManager(x => OnTouch(x), y => OnRaycastTouch(y));
        }
    }
}