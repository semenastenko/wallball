﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace HPEW.Input
{
    public class TouchManager
    {
        Action<TouchType> action;
        Action<GameObject> objAction;

        IDisposable disposable;

        public TouchManager(Action<TouchType> action, Action<GameObject> objAction)
        {
            this.action = action;
            this.objAction = objAction;

            disposable = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        void Update()
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                if (!Raycast())
                    action?.Invoke(TouchType.OneTouch);
            }
        }

        bool Raycast()
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 150)) //, (1 << 14)))
            {
                if (hit.collider.gameObject.layer == 14)
                {
                    objAction?.Invoke(hit.collider.gameObject);
                    return true;
                }
            }
            return false;
        }
    }

    public enum TouchType
    {
        OneTouch,
        DoubleTouch,
        ObjTouch
    }
}
