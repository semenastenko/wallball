﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace HPEW.Input
{
    public class SwipeManager
    {
        // If the touch is longer than MAX_SWIPE_TIME, we dont consider it a swipe
        public float MAX_SWIPE_TIME = 0.5f;

        // Factor of the screen width that we consider a swipe
        // 0.17 works well for portrait mode 16:9 phone
        public float MIN_SWIPE_DISTANCE = 0.02f;

        Vector2 startPos;
        float startTime;

        IDisposable disposableUpdate;

        Action<TypeSwipe> action;

        public SwipeManager(Action<TypeSwipe> action)
        {
            this.action = action;
            disposableUpdate = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        public void Update()
        {
#if UNITY_ANDROID
            if (UnityEngine.Input.touches.Length > 0)
            {
                Touch t = UnityEngine.Input.GetTouch(0);
                if (t.phase == TouchPhase.Began)
                {
                    startPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);
                    startTime = Time.time;
                }
                if (t.phase == TouchPhase.Ended)
                {
                    if (Time.time - startTime > MAX_SWIPE_TIME) // press too long
                        return;

                    Vector2 endPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);

                    Vector2 swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

                    if (swipe.magnitude < MIN_SWIPE_DISTANCE) // Too short swipe
                        return;

                    if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
                    { // Horizontal swipe
                        if (swipe.x > 0)
                        {
                            action(TypeSwipe.Right);
                        }
                        else
                        {
                            action(TypeSwipe.Left);
                        }
                    }
                    else
                    { // Vertical swipe
                        if (swipe.y > 0)
                        {
                            action(TypeSwipe.Up);
                        }
                        else
                        {
                            action(TypeSwipe.Down);
                        }
                    }
                }
            }

#endif

#if UNITY_EDITOR
            if (UnityEngine.Input.GetKeyDown(KeyCode.LeftArrow))
            {
                action(TypeSwipe.Left);
            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.RightArrow))
            {
                action(TypeSwipe.Right);

            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.UpArrow))
            {
                action(TypeSwipe.Up);
            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.DownArrow))
            {
                action(TypeSwipe.Down);
            }
#endif
        }
    }

    public enum TypeSwipe
    {
        Down = 1,
        Up,
        Right,
        Left,
    }
}