﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using HPEW.ShellSystem;

namespace HPEW.DebugTools
{

    public class ShellParamsDisplay : MonoBehaviour
    {
        int width;
        int height;
        Rect rect;
        GUIStyle labelStyle;

        [SerializeField] Color color;

        public bool Show = true;

        [Inject] ShellMotionController _smc;

        void Awake()
        {
            width = Screen.width;
            height = Screen.height;
            rect = new Rect(10, 10, width - 20, height - 20);
        }

        void OnGUI()
        {
            if (!Show) return;

            // Display the label at the center of the window.
            labelStyle = new GUIStyle(GUI.skin.GetStyle("label"));
            labelStyle.alignment = TextAnchor.UpperRight;

            var size = 8;

#if UNITY_ANDROID && !UNITY_EDITOR
            size = 4;
#endif

            // Modify the size of the font based on the window.
            labelStyle.fontSize = size * (width / 200);

            var text = "Текущая скорость: " + _smc.CurrentVelocity.ToString("f3") + "\n\n" +
                "Параметры анимации: \n" +
                "Стартовая скорость: " + _smc.StartSpeed.ToString("f3") + "\n" +
                "Целевая скорость: " + _smc.TargetSpeed.ToString("f3") + "\n" +
                "Ускорение при анимации: " + _smc.MaxAccelerationAnim.ToString("f3") + "\n\n" +

                "Параметры при геймплее: \n" +
                "Максимальная скорость: " + _smc.LimitSpeed.ToString("f3") + "\n" +
                "Ускорение: " + _smc.MaxAcceleration.ToString("f3") + "\n";

            var @default = GUI.contentColor;
            GUI.contentColor = color;
            GUI.Label(rect, text, labelStyle);
            GUI.contentColor = @default;

            //// Obtain the current time.
            //currentTime = Time.time.ToString("f3");
            //currentTime = "Время: " + currentTime + " с.";

            //// Display the current time.
            //GUI.Label(rect, currentTime, labelStyle);
        }
    }
}
