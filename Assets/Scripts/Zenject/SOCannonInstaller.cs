using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "SOCannonInstaller", menuName = "Installers/SOInstaller")]
public class SOCannonInstaller : ScriptableObjectInstaller<SOCannonInstaller>
{
    public HPEW.Systemic.Settings _settings;
    public HPEW.Systemic.Assets _assets;

    public override void InstallBindings()
    {
        Container.BindInstances(_settings, _assets);
    }
}