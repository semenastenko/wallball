using UnityEngine;
using Zenject;
using HPEW.Input;

namespace HPEW.Zenject {

    public class InputSystemInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<InputSystem>()
                .AsSingle()
                .NonLazy();
        }
    }
}