﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SkinsWindow : Window
{
    public override void Init(UIManager UImanager)
    {
        base.Init(UImanager);
    }
    protected override void SelfOpen(Action<Window> callback)
    {
        this.gameObject.SetActive(true);
        ChangeCurrentWindow(this, () =>
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, -Screen.height / 2, gameObject.transform.position.z);
            LeanTween.moveY(gameObject, Screen.height / 2, 0.5f)
                .setEase(LeanTweenType.easeOutBack)
                .setOnComplete(() =>
                {
                    callback?.Invoke(this);
                });
        });
    }

    protected override void SelfClose(Action<Window> callback)
    {
        LeanTween.moveY(gameObject, -Screen.height / 2, 0.5f)
            .setEase(LeanTweenType.easeInOutBack)
            .setOnComplete(() =>
            {
                this.gameObject.SetActive(false);
                callback?.Invoke(this);
                if (!(CurrentWindow is MainMenuWindow))
                {
                    UImanager.Get<MainMenuWindow>().Open();
                }
            });
    }
    protected override void EscapeClose(Action<Window> callback)
    {

    }
}
