﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class Window : MonoBehaviour
{
    protected UIManager UImanager;
    public bool IsOpen { get; private set; }
    public static Window CurrentWindow { get; protected set; } = null;

    public event Action<Window> OnOpen;
    public event Action<Window> OnClose;

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            EscapeClose(null);
    }

    public void Open(Action callback = null)
    {
        Action<Window> handler = delegate
        {
            IsOpen = true;
            OnOpen?.Invoke(this);
            callback?.Invoke();
        };
        SelfOpen(handler);
    }

    protected abstract void SelfOpen(Action<Window> callback);
    protected abstract void SelfClose(Action<Window> callback);
    protected abstract void EscapeClose(Action<Window> callback);
    public virtual void Init(UIManager UImanager)
    {
        this.UImanager = UImanager;
    }

    public void Close(Action callback = null)
    {
        Action<Window> handler = delegate
        {
            IsOpen = false;
            OnClose?.Invoke(this);
            //if (CurrentWindow != null)
            //    CurrentWindow.Close();
            callback?.Invoke();
        };
        SelfClose(handler);
    }


    protected void ChangeCurrentWindow(Window sender, Action callback = null)
    {
        if (CurrentWindow != null)
        {
            CurrentWindow.Close(callback);
        }
        CurrentWindow = sender;
    }
}
