﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameoverWindow : Window
{
    public override void Init(UIManager UImanager)
    {
        base.Init(UImanager);
    }
    protected override void SelfOpen(Action<Window> callback)
    {
        this.gameObject.SetActive(true);
        gameObject.transform.position = new Vector3(-Screen.width / 2, gameObject.transform.position.y, gameObject.transform.position.z);
        LeanTween.moveX(gameObject, Screen.width / 2, 0.5f)
            .setEase(LeanTweenType.easeOutBack)
            .setOnComplete(() =>
            {
                callback?.Invoke(this);
            });
    }

    protected override void SelfClose(Action<Window> callback)
    {
        LeanTween.moveX(gameObject, -Screen.width / 2, 0.5f)
            .setEase(LeanTweenType.easeInOutBack)
            .setOnComplete(() =>
            {
                this.gameObject.SetActive(false);
                callback?.Invoke(this);
            });
    }

    protected override void EscapeClose(Action<Window> callback)
    {
        
    }
}
