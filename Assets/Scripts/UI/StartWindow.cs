﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StartWindow : Window
{
    public override void Init(UIManager UImanager)
    {
        base.Init(UImanager);
        //CurrentWindow = this;
    }

    protected override void SelfOpen(Action<Window> callback)
    {
        this.gameObject.SetActive(true);
        callback?.Invoke(this);
    }

    protected override void SelfClose(Action<Window> callback)
    {
        this.gameObject.SetActive(false);
        callback?.Invoke(this);
    }

    protected override void EscapeClose(Action<Window> callback)
    {

    }
}
