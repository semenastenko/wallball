﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UIManager : IInitializable
{
    private Canvas _canvas;
    public List<Window> Windows { get; private set; }

    public UIManager([Inject(Id = "UI")] Canvas canvas)
    {
        _canvas = canvas;
        Windows = new List<Window>();
        Windows.AddRange(canvas.GetComponentsInChildren<Window>(true));
        foreach (var window in Windows)
            window.Init(this);
    }

    public void Initialize()
    {
        Debug.Log("win " + Windows.Count);
        foreach (var window in Windows)
        {

            if (window is StartWindow)
                window.Open();
            else
                window.Close();
        }
    }

    public T Get<T>() where T : Window
    {
        foreach (var window in Windows)
        {
            if (window is T)
                return window as T;
        }
        return null;
    }
}
