﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;
using UnityEditor;

public enum ConfirmType
{
    ok, yes, no
}
public class ConfirmationButton : Button
{
    public ConfirmType confirmType;
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        Window parent = GetComponentInParent<Window>();

        MessageBroker.Default
            .Publish(GameManager.ConfirmMessage.Create(confirmType, parent));
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ConfirmationButton))]
public class ConfirmationButtonEditor : UnityEditor.UI.ButtonEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        ConfirmationButton targetButton = (ConfirmationButton)target;

        targetButton.confirmType = (ConfirmType)EditorGUILayout.EnumPopup("Change confirmation", targetButton.confirmType);

        // Show default inspector property editor
    }
}
#endif
