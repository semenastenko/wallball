﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DestroyIt;
using System;

namespace HPEW.Systemic
{
    [CreateAssetMenu(fileName = "Settings", menuName = "System/Settings", order = 51)]
    public class Settings : ScriptableObject
    {
        #region Game Design

        [Header("Game Design")]
        [SerializeField] float shotTime;
        public float ShotTime => shotTime;

        [SerializeField, Range(0.001f, 100f)]
        float speedDissolveDestructibleLet = 0.001f;
        public float SpeedDissolveDestructibleLet => speedDissolveDestructibleLet;

        [SerializeField, Range(0.001f, 100f)]
        float speedDissolveSwipeLet = 0.001f;
        public float SpeedDissolveSwipeLet => speedDissolveSwipeLet;

        [SerializeField, Range(0.001f, 100f)]
        float speedDissolveChunk = 0.001f;
        public float SpeedDissolveChunk => speedDissolveChunk;

        #endregion

        #region Chunk Generation Settings

        [Header("Chunk Generation Settings")]
        [SerializeField] int countInitChunks = 0;
        public int CountInitChunks => countInitChunks;

        [SerializeField] int maxCountChunks = 1;
        public int MaxCountChunks => maxCountChunks;

        [SerializeField, Range(0f, 1000f)] float spawnDistance = 100f;
        public float SpawnDistance => spawnDistance;

        [SerializeField, Range(10f, 100f)] int intervalLets = 10;
        public int IntervalLets => intervalLets;

        #endregion

        #region Buster Generation Settings

        [Header("Buster Generation Settings")]
        [SerializeField] int busterPerChunk = 1;
        public int BusterPerChunk => busterPerChunk;

        [SerializeField] float coinSpawnСhance = 1;
        public float CoinSpawnСhance => coinSpawnСhance;

        [SerializeField] float busterSpawnСhance = 1;
        public float BusterSpawnСhance => busterSpawnСhance;

        //[SerializeField, Range(0f, 1000f)] float spawnDistance = 100f;
        //public float SpawnDistance => spawnDistance;

        //[SerializeField, Range(10f, 100f)] int intervalLets = 10;
        //public int IntervalLets => intervalLets;

        #endregion

        #region Pool Settings

        [Header("Pool Settings")]
        [SerializeField] int initialSize = 4;
        public int InitialSize => initialSize;

        #endregion

        [Header("Level Damages"), SerializeField]
        private List<StartDamageLevel> list;
        public List<DamageLevel> GetDamageLevels(int maxHP)
            => Array.Find(list.ToArray(), start => start.maxHP == maxHP).list;

    }

    [Serializable]
    public struct StartDamageLevel
    {
        public int maxHP;
        public List<DamageLevel> list;
    }

}