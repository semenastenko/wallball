﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPEW.ChunkSystem;
using HPEW.Interactive;
using HPEW.ShellSystem;


namespace HPEW.Systemic
{
    [CreateAssetMenu(fileName = "Assets", menuName = "System/Assets", order = 52)]
    public class Assets : ScriptableObject
    {
        [SerializeField] private GameObject _hpPopup;
        public GameObject HpPopup => _hpPopup;

        [SerializeField] private Chunk[] _chunkPrefabs;
        public Chunk[] ChunkPrefabs => _chunkPrefabs;

        [SerializeField] private Let[] _letPrefabs;
        public Let[] LetPrefabs => _letPrefabs;

        [SerializeField] private Buster[] _busterPrefabs;
        public Buster[] BusterPrefabs => _busterPrefabs;
    }
}