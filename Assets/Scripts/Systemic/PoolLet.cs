﻿using UnityEngine;
using Zenject;
using HPEW.Interactive;


namespace HPEW.Pool
{
    public class PoolLet : MonoMemoryPool<Let>
    {
        int index = 0;

        protected override void OnCreated(Let item)
        {
            base.OnCreated(item);
            item.name += " " + index; 
            index++;
        }

        protected override void Reinitialize(Let let)
        {
            let.Restart();
        }

        protected override void OnDespawned(Let let)
        {
            base.OnDespawned(let);
            let.Sleep();
        }
    }
}